import Button from './Button';
import { Row, Col } from 'react-bootstrap';


export default function CarouselSection() {
    return (
        <div className="my-5">
            <Row>
                <Col xs={12} md={7} className="p-0">
                    <img
                        className="img-fluid"
                        src={require('../images/featured1.png')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3">
                        <h4>Japanese Bike</h4>
                        <p className="w-lg-75 gray">
                            The most popular Japan Bikes or Vintage Bikes in the Philippines.
                        </p>
                        <Button
                        id='6245939a3f55e168e88041a8'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Learn More
                        </Button>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={7} className="p-0 order-lg-2">
                    <img
                        className="img-fluid"
                        src={require('../images/featured2.png')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3 order-lg-1">
                        <h4>Vans</h4>
                        <p className="w-lg-75 gray">
                            The Old Skool, Vans classic skate shoe and the first to bear the iconic side stripe.
                        </p>
                        <Button
                        id='62459aa63f55e168e88041f8'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Learn More
                        </Button>
                    </div>
                </Col>
            </Row>            
            <Row>
                <Col xs={12} md={7} className="p-0">
                    <img
                        className="img-fluid"
                        src={require('../images/featured3.png')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3">
                        <h4>Samsung Galaxy S22</h4>
                        <p className="w-lg-75 gray">
                            Defy the rules and discover a new everyday with Galaxy S22
                        </p>
                        <Button
                        id='62459d4a3f55e168e880421c'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Learn More
                        </Button>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={7} className="p-0 order-lg-2">
                    <img
                        className="img-fluid"
                        src={require('../images/featured4.png')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3 order-lg-1">
                        <h4>Cervello Bikes</h4>
                        <p className="w-lg-75 gray">
                            The road calls. From high mountain peaks to neighborhood streets and bike paths, Giant road bikes for men are your invitation to ride.
                        </p>
                        <Button
                        id='6245a0433f55e168e880422f'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Learn More
                        </Button>
                    </div>
                </Col>
            </Row>       
        </div>
    )
}