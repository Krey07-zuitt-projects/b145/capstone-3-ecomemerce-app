import '../design/Footer.css'

export default function Footer() {
	return  (
		<div className="footer-container">
				<big className="website-rights social-media-items">KREY STORE © 2022</big>
		</div>
	)
}