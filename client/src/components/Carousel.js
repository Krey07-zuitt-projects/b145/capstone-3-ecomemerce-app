import Carousel from 'react-bootstrap/Carousel';
import '../design/Carousel.css';

export default function CarouselSection() {
    return (
        <Carousel variant="light" controls={false} fade>
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 carouselImg"
                    src={require('../images/carousel1.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1>Sunpeed Kepler Gravel Bike</h1>
                    <h5>Built for all roads</h5>
                </div>
            </Carousel.Item>            
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 carouselImg"
                    src={require('../images/carousel2.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1>Air Jordan 1 Dior</h1>
                    <h5>Limited Edition</h5>
                </div>
            </Carousel.Item>            
            <Carousel.Item className="carouselFit" interval={1500}>
                <img
                    className="d-block w-100 carouselImg"
                    src={require('../images/carousel3.jpg')}
                    alt="First slide"
                />
                <div className="overlay">
                    <h1>iPhone 13 Pro Max</h1>
                    <h5>Future of Smartphones</h5>
                </div>
            </Carousel.Item>
        </Carousel>
    )
}