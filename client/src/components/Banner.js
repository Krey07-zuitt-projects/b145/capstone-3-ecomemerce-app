import { Fragment } from 'react';

export default function Banner() {
    return (
        <Fragment>
            <div className="d-flex flex-column align-items-center justify-content-center text-center my-5 mx-lg-5">
                <h2>FEATURED PRODUCTS</h2>
            </div>
        </Fragment>
    )
}